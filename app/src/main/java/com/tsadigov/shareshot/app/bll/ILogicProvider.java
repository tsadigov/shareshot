package com.tsadigov.shareshot.app.bll;

/**
 * Created by tural on 8/10/14.
 */
public interface ILogicProvider {
    public UiLogic getLogic();
}
