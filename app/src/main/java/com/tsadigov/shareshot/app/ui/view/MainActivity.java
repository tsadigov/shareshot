package com.tsadigov.shareshot.app.ui.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;

import com.tsadigov.shareshot.app.R;
import com.tsadigov.shareshot.app.bll.ILogicProvider;
import com.tsadigov.shareshot.app.bll.IResultConsumer;
import com.tsadigov.shareshot.app.bll.UiLogic;
import com.tsadigov.shareshot.app.model.Shot;
import com.tsadigov.shareshot.app.ui.IContainer;
import com.tsadigov.shareshot.app.ui.IUserNavigationCoordinator;
import com.tsadigov.shareshot.app.ui.OnComplete;

import java.io.File;
import java.util.UUID;


public class MainActivity extends ActionBarActivity
        implements IContainer, IUserNavigationCoordinator,ILogicProvider {


    UiLogic _uiLogic;
public  MainActivity(){
    _uiLogic=new UiLogic(this);
}

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);


        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

/*
//TODO:refactor to more model based structure, called by fragments from onAttach
    public void onSectionAttached(int number) {
        switch (number) {
            case 1:
                mTitle = "Shot List";
                break;
            case 2:
                mTitle = "Album List";
                break;
            case 3:
                mTitle = "Foo";
                break;
        }
    }
*/
    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    private static int TAKE_PICTURE = 1;
    private Uri outputFileUri;
    private String filePath;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_camera) {

            Camera();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void Camera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File dir=getExternalFilesDir("Shots");
        dir.mkdirs();

        File file = new File(dir,
                UUID.randomUUID().toString()+".jpg");
        outputFileUri = Uri.fromFile(file);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        startActivityForResult(intent, TAKE_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {
        if (requestCode == TAKE_PICTURE) {
            Uri imageUri = null;
// Check if the result includes a thumbnail Bitmap
            if (data != null) {
                if (data.hasExtra("data")) {
                    Bitmap thumbnail = data.getParcelableExtra("data");
// TODO Do something with the thumbnail, currently not using this
                }
            }
            else {
                saveNewCapture();


            }
        }
    }

    /**
     * takes the bitmap that camera app cerated and adds it to the database
     */
    private void saveNewCapture() {
        //be carefull not use toString , use toPath
        Shot shot=_uiLogic.newShot(outputFileUri.getPath());
        GotoEdit(shot);

    }


    @Override
    public void navigate(Fragment fragment) {

        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction tran=fragmentManager.beginTransaction();
        tran.replace(R.id.container,fragment );
        tran.addToBackStack(null);
        tran.commitAllowingStateLoss();
    }


    @Override
    public void GotoEdit(Shot shot) {
        Fragment fragment = EditShot.newInstance(shot);
        navigate(fragment);
    }

    public  void GotoShotList(){
        navigate(ImageListFragment.newInstance());
    }


    @Override
    public void GotoAlbumList() {
        navigate(AlbumListFragment.newInstance());
    }

    @Override
    public void GotoMap() {
        //TODO: dummy implementation
        navigate(PlaceholderFragment.newInstance(0));
    }

    @Override
    public IResultConsumer<Boolean>[] getNavigationOptions() {
        return new IResultConsumer[]{
                OnComplete.GotoShotList(this),
                OnComplete.GotoAlbumList(this),
                OnComplete.GotoMap(this)
        };
    }

    @Override
    public void GoBack() {
        this.getSupportFragmentManager().popBackStack();
    }

    @Override
    public UiLogic getLogic() {
        return _uiLogic;
    }
}
