package com.tsadigov.shareshot.app.model;

import android.content.ContentValues;
import android.database.Cursor;

import java.util.UUID;

/**
 * Entity describing camera shots
 * Created by tural on 8/7/14.
 */
public class Shot {

    public Shot(){
        id= UUID.randomUUID().toString();
    }
    //<editor-fold desc="fields">
    /**PK*/
    public String id;

    /**Geolocation the picture was taken*/
    public double lat;
    /**Geolocation the picture was taken*/
    public double lon;

    public String title;

    /**Path of the image file in the storage*/
    public String filePath;


    //TODO: datetime

    // </editor-fold>


    @Override
    public String toString() {
        return title;
    }

    /**metadata singleton instance*/
    public final static  ShotMetadata metadata=new ShotMetadata();

    /**Metadata specific to entity Shot*/
    private static class ShotMetadata extends EntityMetadata{

        private ShotMetadata() {
            super("Shot");
        }

        @Override
        public String generateCreateSql() {
            return "CREATE TABLE Shot (id VARCHAR(32),title VARCHAR(64),filePath VARCHAR(256),lat NUMERIC, lon NUMERIC)";
        }

        @Override
        public Object decode(Cursor cursor) {

            Shot entity=new Shot();
            //TODO: index works faster but dont hardcode them
            entity.id=cursor.getString(0);
            entity.title=cursor.getString(1);
            entity.filePath=cursor.getString(2);
            entity.lat=cursor.getDouble(3);
            entity.lon=cursor.getDouble(4);
            return entity;
        }

        @Override
        public ContentValues encode(Object oentity) {
            Shot entity=(Shot)oentity;
            ContentValues val=new ContentValues();
            val.put("id",entity.id);
            val.put("title",entity.title);
            val.put("lat",entity.lat);
            val.put("lon",entity.lon);
            val.put("filePath",entity.filePath);

            return val;
        }


        @Override
        public String getPkName() {
            return "id";
        }

        @Override
        public String getPkValue(Object oentity) {
            Shot entity=(Shot)oentity;

            return entity.id;
        }

    }
}
