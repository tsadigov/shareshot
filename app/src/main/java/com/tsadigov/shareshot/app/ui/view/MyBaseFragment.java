package com.tsadigov.shareshot.app.ui.view;

import android.app.Activity;
import android.support.v4.app.Fragment;
import com.tsadigov.shareshot.app.bll.ILogicProvider;
import com.tsadigov.shareshot.app.bll.UiLogic;
import com.tsadigov.shareshot.app.ui.IUserNavigationCoordinator;

/**
 * Created by tural on 8/10/14.
 */
public class MyBaseFragment extends Fragment {

    protected IUserNavigationCoordinator _navigation;
    protected UiLogic _logic;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        _navigation =(IUserNavigationCoordinator)activity;
        _logic=((ILogicProvider)getActivity()).getLogic();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        _navigation=null;
        _logic=null;
    }

}
