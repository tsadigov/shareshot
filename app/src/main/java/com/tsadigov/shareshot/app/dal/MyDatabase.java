package com.tsadigov.shareshot.app.dal;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.tsadigov.shareshot.app.model.Album;
import com.tsadigov.shareshot.app.model.EntityMetadata;
import com.tsadigov.shareshot.app.model.Shot;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by tural on 8/7/14.
 */
public class MyDatabase extends SQLiteOpenHelper{

    private MyDatabase(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    Collection<EntityMetadata> _entityDefinitionCollection=new ArrayList<EntityMetadata>();
    protected void setEntityDefinitionCollection(Collection<EntityMetadata> entityDefinitionCollection) {
        this._entityDefinitionCollection = entityDefinitionCollection;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        for(EntityMetadata entityDefinition: _entityDefinitionCollection)
            db.execSQL(entityDefinition.generateCreateSql());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //TODO:implement db schema upgrade

    }

    public void read(IDataTransaction action){
        SQLiteDatabase connection=null;

        try{
            connection = getReadableDatabase();
            action.execute(connection);
        }
        catch (Exception exp){
            Log.e("data access", exp.getMessage(), exp);
        }
        finally {
            connection.close();
            action.ready();
        }
    }

    public void write(IDataTransaction action){
        SQLiteDatabase connection=null;

        try{
            connection = getWritableDatabase();
            action.execute(connection);
        }
        catch (Exception exp){
            Log.e("data access", exp.getMessage(), exp);
        }
        finally {
            connection.close();
            action.ready();
        }
    }
    /**
     * factory method
     */

    public static MyDatabase getInstance(Context ctx){
        Collection<EntityMetadata> entityDefinitionList=new ArrayList<EntityMetadata>();
        entityDefinitionList.add(Shot.metadata);
        entityDefinitionList.add(Album.metadata);

        return getInstance(ctx,entityDefinitionList);
    }

    public static MyDatabase getInstance(Context ctx,Collection<EntityMetadata> entityDefinitionCollection){
        MyDatabase db=new MyDatabase(ctx, DB_NAME, null, DB_VERSION);
        db.setEntityDefinitionCollection(entityDefinitionCollection);
        return db;
    }

    /**
     * settings for database
     */
    static int DB_VERSION=1;
    static String DB_NAME="ShareShoot";

}
