package com.tsadigov.shareshot.app.model;

import android.content.ContentValues;
import android.database.Cursor;

/**
 * Created by tural on 8/7/14.
 */
public class Album {
    public String id;
    public String title;

    public static final EntityMetadata metadata=new AlbumMetadata();

    private static class AlbumMetadata extends EntityMetadata{

        protected AlbumMetadata() {
            super("Album");
        }

        @Override
        public String generateCreateSql() {
            return "CREATE TABLE Album(id VARCHAR(32), title VARCHAR(64));";
        }

        @Override
        public Object decode(Cursor cursor) {
            Album entity=new Album();
            entity.id=cursor.getString(0);
            entity.title=cursor.getString(1);
            return  entity;
        }

        @Override
        public ContentValues encode(Object oentity) {
            Album entity=(Album) oentity;
            ContentValues values=new ContentValues();

            values.put("id", entity.id);
            values.put("title", entity.title);
            return null;
        }

        @Override
        public String getPkName() {
            return "id";
        }

        @Override
        public String getPkValue(Object oentity) {
            Album entity=(Album)oentity;

            return entity.id;
        }
    }
}
