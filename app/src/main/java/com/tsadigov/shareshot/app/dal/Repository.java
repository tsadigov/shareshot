package com.tsadigov.shareshot.app.dal;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.tsadigov.shareshot.app.model.EntityMetadata;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tural on 8/7/14.
 */
public class Repository<TEntity> {

    EntityMetadata _metadata;

    public Repository(EntityMetadata metadata){
        _metadata=metadata;
    }


    public long insert(TEntity entity, SQLiteDatabase connection){
        return connection.insertOrThrow(_metadata.getName(), null, _metadata.encode(entity));
    }




    public ArrayList<TEntity> all(SQLiteDatabase connection,String order) {
        StringBuilder str=new StringBuilder(60);
        str
            .append("SELECT * FROM ")
            .append(_metadata.getName());
        
        if(null!=order)
            str
                .append(" ORDER BY ")
                .append(order);

        final Cursor cur = connection.rawQuery(str.toString(),new String[0]);
        ArrayList<TEntity> results =this.all(cur);
        return results;
    }

    public ArrayList<TEntity> where(String where, String sort, SQLiteDatabase connection) {
        String sql="SELECT * FROM "
                + _metadata.getName()
                + ((null==where)?" ":(" WHERE "+where))
                +((null==sort)?" ":(" ORDER BY  "+sort));

        final Cursor cur = connection.rawQuery(sql , new String[0]);

        return all(cur);
    }

    protected ArrayList<TEntity> all(Cursor cur) {
        ArrayList<TEntity> results =new ArrayList<TEntity>();
        cur.moveToFirst();
        for(int idx=0;idx<cur.getCount();idx++){
            results.add((TEntity)_metadata.decode(cur));
            cur.moveToNext();
        }
        return results;
    }

    public TEntity single(String where, SQLiteDatabase connection) {
        final Cursor cur = connection.rawQuery("SELECT * FROM " + _metadata.getName() + ((null==where)?"":("WHERE "+where)), new String[0]);
        return single(cur);
    }

    private TEntity single(Cursor cur) {
        if(cur.moveToFirst()){
            return (TEntity)_metadata.decode(cur);
        }
        return null;
    }

    public void update(TEntity entity, SQLiteDatabase connection) {
        ContentValues cv = _metadata.encode(entity);

        connection.update(_metadata.getName(), cv, _metadata.getPkName()+"=?",new String[]{_metadata.getPkValue(entity)});
    }


    public void delete(TEntity entity, SQLiteDatabase connection) {
        ContentValues cv = _metadata.encode(entity);

        connection.delete(_metadata.getName(), _metadata.getPkName()+"=?",new String[]{_metadata.getPkValue(entity)});
    }


}
