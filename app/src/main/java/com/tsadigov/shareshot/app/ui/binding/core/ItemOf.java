package com.tsadigov.shareshot.app.ui.binding.core;

import android.view.View;

import com.tsadigov.shareshot.app.model.Shot;

/**
 * Created by tural on 8/10/14.
 */
public abstract class ItemOf<TModel> extends ListItemBinder {
    protected ItemOf(View rootView) {
        super(rootView);
    }

    @Override
    public abstract void populate() ;

    TModel _model;
    protected TModel getModel() {
        return _model;
    }
    public void bindTo(final TModel model){
        if(model==this._model)
            return;

        this._model=model;
        populate();
    }

}
