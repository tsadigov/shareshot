package com.tsadigov.shareshot.app.ui.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.tsadigov.shareshot.app.R;

import com.tsadigov.shareshot.app.bll.IResultConsumer;
import com.tsadigov.shareshot.app.model.Shot;
import com.tsadigov.shareshot.app.ui.binding.core.ItemOf;
import com.tsadigov.shareshot.app.ui.binding.ShotBinder;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p />
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p />
 * interface.
 */
public class ImageListFragment extends MyBaseFragment  implements AdapterView.OnItemClickListener {


    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private ListAdapter mAdapter;

    // TODO: Rename and change types of parameters
    public static ImageListFragment newInstance() {
        ImageListFragment fragment = new ImageListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ImageListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
        }


        // TODO: Change Adapter to display your content
        //mAdapter = new ArrayAdapter<Shot>(getActivity(),
        //        R.layout.item_shot, R.id.title, new ArrayList<Shot>());
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_imagelist, container, false);

        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);

        _logic.getShots(new IResultConsumer<ArrayList<Shot>>() {
            @Override
            public void supply(ArrayList<Shot> argument) {
                mAdapter = new ArrayAdapter<Shot>(getActivity(),
                        R.layout.item_shot, R.id.title, argument){
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        if(null==convertView)
                            convertView=inflater.inflate(R.layout.item_shot,parent,false);

                        ItemOf<Shot> binder=(ItemOf<Shot>)convertView.getTag();
                        if(null==binder){
                            binder=new ShotBinder(convertView);
                            Shot shot=(Shot)mAdapter.getItem(position);
                            binder.bindTo(shot);
                        }
                        binder.populate();

                        return convertView;
                    }
                };

                ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

            }
        });

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return view;
    }



    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != _navigation) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            Shot item=(Shot)mAdapter.getItem(position);
            _navigation.GotoEdit(item);
        }
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyText instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }


}
