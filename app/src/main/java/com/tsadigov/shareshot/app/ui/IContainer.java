package com.tsadigov.shareshot.app.ui;


import android.support.v4.app.Fragment;

/**
 * Created by tural on 8/8/14.
 */
public interface IContainer {
    public void navigate(Fragment fragment);
}
