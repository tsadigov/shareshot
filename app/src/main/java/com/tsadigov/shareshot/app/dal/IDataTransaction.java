package com.tsadigov.shareshot.app.dal;

import android.database.sqlite.SQLiteDatabase;

/**
 * Created by tural on 8/8/14.
 */
public interface IDataTransaction {
    void execute(SQLiteDatabase connection);

    void ready();
}
