package com.tsadigov.shareshot.app.dal;

import com.tsadigov.shareshot.app.model.Album;
import com.tsadigov.shareshot.app.model.Shot;

/**
 * Created by tural on 8/8/14.
 *
 * Repository instances
 */
public class ListOf {
    public static final Repository<Shot> shots=new Repository<Shot>(Shot.metadata);
    public static final Repository<Album> albums=new Repository<Album>(Album.metadata);
}
