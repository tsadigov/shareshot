package com.tsadigov.shareshot.app.model;

import android.content.ContentValues;
import android.database.Cursor;

/**
 * Metadata describing entities structure
 * Created by tural on 8/7/14.
 * 
 */
public abstract class EntityMetadata  {
    public EntityMetadata(String pName){
        this._name=pName;
    }


    String _name;
    public String getName() {
        return _name;
    }
    
    public abstract String generateCreateSql();
    public abstract Object decode(Cursor cursor);
    public abstract ContentValues encode(Object entity);

    public abstract String getPkName();
    public abstract String getPkValue(Object entity);

}
