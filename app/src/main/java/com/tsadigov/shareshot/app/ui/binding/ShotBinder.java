package com.tsadigov.shareshot.app.ui.binding;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tsadigov.shareshot.app.R;
import com.tsadigov.shareshot.app.model.Shot;
import com.tsadigov.shareshot.app.ui.binding.core.ItemOf;
import com.tsadigov.shareshot.app.util.BitmapUtils;

/**
 * Created by tural on 8/10/14.
 */
public class ShotBinder extends ItemOf<Shot> {
    TextView _title;
    ImageView _img;

    Bitmap _bitmap;
    Bitmap getBitmap(){
        if(null==_bitmap)
            _bitmap=BitmapUtils.loadThumbnail(getModel());
        return _bitmap;
    }

    public ShotBinder(View rootView) {
        super(rootView);
    }

    @Override
    public void populate() {
        _img.setImageBitmap(getBitmap());
        _title.setText(getModel().title);
    }

    @Override
    protected void view() {
        _title=(TextView)getRoot().findViewById(R.id.title);
        _img=(ImageView)getRoot().findViewById(R.id.img);
    }

}
