package com.tsadigov.shareshot.app.ui;

import com.tsadigov.shareshot.app.bll.IResultConsumer;
import com.tsadigov.shareshot.app.model.Album;
import com.tsadigov.shareshot.app.model.Shot;

/**
 * Created by tural on 8/8/14.
 */
public interface IUserNavigationCoordinator {

    public void GotoEdit(Shot shot);

    void GotoShotList();

    void GotoAlbumList();

    void GotoMap();

    /** main navigation options for drawer*/
    IResultConsumer[] getNavigationOptions();

    void GoBack();

}
