package com.tsadigov.shareshot.app.ui.binding.core;

import android.view.View;

/**
 * Created by tural on 8/10/14.
 */
public abstract class ListItemBinder {
    View _root;
    protected View getRoot(){
        return _root;
    }

    protected ListItemBinder(View rootView){
        _root=rootView;
        rootView.setTag(this);
        view();
    }
    public abstract void populate();

    /**get references to controls to use later*/
    protected void view(){

    }
}
