package com.tsadigov.shareshot.app.ui.view;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.tsadigov.shareshot.app.R;
import com.tsadigov.shareshot.app.model.Shot;
import com.tsadigov.shareshot.app.ui.IUserNavigationCoordinator;
import com.tsadigov.shareshot.app.ui.OnComplete;
import com.tsadigov.shareshot.app.util.BitmapUtils;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EditShot.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EditShot#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class EditShot extends MyBaseFragment {

    Shot _data;

    public void setData(Shot shot){
        _data =shot;
    }

    public Shot getData(){
        return _data;
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param shot Parameter 1.
     * @return A new instance of fragment EditImage.
     */
    // TODO: Rename and change types and number of parameters
    public static EditShot newInstance(Shot shot) {
        EditShot fragment = new EditShot();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setData(shot);
        return fragment;
    }
    public EditShot() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }


    EditText _title;
    ImageView _img;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        View root= inflater.inflate(R.layout.fragment_edit_shot, container, false);

        try {
        _title=(EditText)root.findViewById(R.id.title);
        _img=(ImageView)root.findViewById(R.id.img);


        _title.setText(getData().title);

            _img.setImageBitmap(BitmapUtils.loadBitmap(getData()));
        }
        catch (Exception exp){
            Log.e(TAG,exp.getMessage());

        }
        return root;
    }

    static final String TAG="EditShot";

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.edit_shot, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_accept:
                Accept();
                break;
            case R.id.action_cancel:
                Cancel();
                break;
            case R.id.action_discard:
                Delete();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
            return true;
    }

    void Accept(){
        final Shot shot=getData();
        shot.title=_title.getText().toString();

        _logic.update(shot, OnComplete.GoBack(_navigation));


    }
    void Cancel(){
        _navigation.GoBack();
    }
    void Delete(){
        final Shot shot=getData();
        _logic.delete(shot,OnComplete.GoBack(_navigation));
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
