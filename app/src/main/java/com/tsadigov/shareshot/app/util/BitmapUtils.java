package com.tsadigov.shareshot.app.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;

import com.tsadigov.shareshot.app.model.Shot;

import java.io.File;


/**
 * Created by tural on 8/8/14.
 */
public class BitmapUtils {
    private static final int THUMBNAIL_SCALE = 8;

    public static Bitmap loadBitmap(Shot shot) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeFile(shot.filePath,options);
    }

    public static Bitmap loadThumbnail(Shot shot) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        options.inSampleSize=THUMBNAIL_SCALE;
        return BitmapFactory.decodeFile(shot.filePath,options);
    }
}
