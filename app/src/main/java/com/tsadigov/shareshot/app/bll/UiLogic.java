package com.tsadigov.shareshot.app.bll;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.tsadigov.shareshot.app.dal.ListOf;
import com.tsadigov.shareshot.app.dal.IDataTransaction;
import com.tsadigov.shareshot.app.dal.MyDatabase;
import com.tsadigov.shareshot.app.model.Album;
import com.tsadigov.shareshot.app.model.Shot;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by tural on 8/10/14.
 */
public class UiLogic {
    MyDatabase _db;
    Context _context;
    public UiLogic(MyDatabase db){
        _db=db;
    }

    public UiLogic(Context context) {
        _context=context;
        _db=MyDatabase.getInstance(_context);
    }

    public void getShots(final IResultConsumer<ArrayList<Shot>> consumer){
        _db.read(new IDataTransaction() {
            ArrayList<Shot> _data;
            @Override
            public void execute(SQLiteDatabase connection) {
                _data= ListOf.shots.all(connection, null);
            }

            @Override
            public void ready() {
                consumer.supply(_data);
            }
        });
    }

    static String TAG = "logic";


    static void testPath(String path,Context _context){
        AssetManager mgr=_context.getAssets();
        String []files=null;

        File file=new File(path);
        Log.i(TAG,path);
        Log.i(TAG, "exists "+file.exists());

        try {
            files=mgr.list(file.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "files: "+files);

    }
    static String parent(String Path){
        File f=new File(Path);
        return  f.getParent();
    }

    public Shot newShot(String path) {
/*

        String tp=path;
        for(int i=0;i<5;i++){
            testPath(tp,_context);
            tp=parent(tp);
        }

    //    testPath(_context.getExternalFilesDir(null).getPath(),_context);
    //    testPath(_context.getExternalFilesDir("Shots").getPath(),_context);
*/
        final Shot shot=new Shot();
        shot.filePath=path;
        shot.title="new CamShot";

        _db.write(new IDataTransaction() {
            @Override
            public void execute(SQLiteDatabase connection) {
                ListOf.shots.insert(shot, connection);
            }

            @Override
            public void ready() {

            }
        });

        return shot;
    }

    public void getAlbums(IResultConsumer<ArrayList<Album>> iTaskComplete) {

    }

    public void delete(final Shot shot, final IResultConsumer<Boolean> booleanITaskComplete) {
        _db.write(new IDataTransaction() {
            @Override
            public void execute(SQLiteDatabase connection) {
                ListOf.shots.delete(shot, connection);
            }

            @Override
            public void ready() {
                booleanITaskComplete.supply(true);
            }
        });
    }

    public void update(final Shot shot, final IResultConsumer<Boolean> booleanITaskComplete) {
        _db.write(new IDataTransaction() {
            @Override
            public void execute(SQLiteDatabase connection) {
                ListOf.shots.update(shot, connection);
            }

            @Override
            public void ready() {
                booleanITaskComplete.supply(true);
            }
        });
    }
}
