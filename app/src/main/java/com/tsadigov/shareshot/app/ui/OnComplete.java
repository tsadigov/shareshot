package com.tsadigov.shareshot.app.ui;

import com.tsadigov.shareshot.app.bll.IResultConsumer;

/**
 * Created by tural on 8/10/14.
 * Utility class that contains ready made navigation task instances
 */
public class OnComplete {
    public static IResultConsumer<Boolean> GotoShotList(final IUserNavigationCoordinator navigation){
        return new IResultConsumer<Boolean>() {
            @Override
            public void supply(Boolean argument) {
                navigation.GotoShotList();
            }

            @Override
            public String toString() {
                return "Shot List";
            }
        };
    }

    public static IResultConsumer<Boolean> GotoAlbumList(final IUserNavigationCoordinator navigation){
        return new IResultConsumer<Boolean>() {
            @Override
            public void supply(Boolean argument) {
                navigation.GotoAlbumList();
            }


            @Override
            public String toString() {
                return "Album List";
            }
        };
    }

    public static IResultConsumer<Boolean> GotoMap(final IUserNavigationCoordinator navigation){
        return new IResultConsumer<Boolean>() {
            @Override
            public void supply(Boolean argument) {
                navigation.GotoMap();
            }


            @Override
            public String toString() {
                return "Map";
            }
        };
    }

    /**Pop backstack*/
    public static IResultConsumer<Boolean> GoBack(final IUserNavigationCoordinator navigation) {
        return new IResultConsumer<Boolean>() {
            @Override
            public void supply(Boolean argument) {
                navigation.GoBack();
            }
        };
    }
}
